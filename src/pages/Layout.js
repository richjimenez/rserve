import React from 'react';
import { Layout, Menu } from 'antd';
import { UploadOutlined, UserOutlined, ReadOutlined, CoffeeOutlined } from '@ant-design/icons';
import Menus from '../pages/Menu';

const { Header, Content, Footer, Sider } = Layout;

export default () => {
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={broken => {
          // console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          // console.log(collapsed, type);
        }}
      >
        <div className="logo" />
        <Menu theme="dark" mode="inline">
          <Menu.Item key="1">
            <ReadOutlined />
            <span className="nav-text">Reservas</span>
          </Menu.Item>
          <Menu.Item key="2">
            <CoffeeOutlined />
            <span className="nav-text">Menu</span>
          </Menu.Item>
        </Menu>
      </Sider>

      <Layout>
        <Header className="site-layout-sub-header-background" style={{ padding: 0 }} />
        <Content style={{ margin: '24px 16px 0' }}>
          <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
            <Menus />
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Rserve ©2020 Rick Jimenez</Footer>
      </Layout>
    </Layout>
  );
};
