import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Table, Button, Avatar, Row, Col, Typography } from 'antd';
import { DeleteOutlined, PlusOutlined, LogoutOutlined } from '@ant-design/icons';
import firebase, { db, storage, auth } from '../firebase';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { setMenu, removeMenuItem } from '../redux/menu/actions';

import AddMenuItem from '../components/AddMenuItem';
import Categories from '../components/Categories';
import CategoriesNav from '../components/CategoriesNav';

const { Title } = Typography;

export default () => {
  const { category } = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [showAddMenuItem, setShowAddMenuItem] = useState(false);
  const [currentTurn, setCurrentTurn] = useState(null);
  const categories = useSelector(state => state.menu.categories);
  const user = useSelector(state => state.auth.currentUser);
  const dispatch = useDispatch();

  const getCatChildren = id => {
    const res = [id];
    const getChildren = parentIds => {
      const children = [];
      parentIds.forEach(parentId => {
        const parents = categories.filter(cat => cat.parent === parentId);
        parents.forEach(cat => {
          res.push(cat.id);
          children.push(cat.id);
        });
        getChildren(children);
      });
    };
    getChildren([id]);
    return res;
  };

  // filter menu items by category and his children
  let menuItems;
  if (category) {
    const res = [];
    const items = useSelector(state => state.menu.items);
    getCatChildren(category).forEach(cat => {
      const [item] = items.filter(item => item.category.id === cat);
      if (item) res.push(item);
    });
    menuItems = res;
  } else {
    menuItems = useSelector(state => state.menu.items);
  }

  const columns = [
    {
      title: 'Imagen',
      dataIndex: 'key',
      render: val => {
        const [item] = menuItems.filter(item => item.key === val);
        return <Avatar shape="square" size="large" src={item.images.s} />;
      },
    },
    {
      title: 'Categoria',
      dataIndex: 'category',
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: val => val.name,
    },
    {
      title: 'Nombre',
      dataIndex: 'name',
      sorter: (a, b) => a.name.localeCompare(b.name),
    },
    {
      title: 'Precio',
      dataIndex: 'price',
      sorter: (a, b) => a.price - b.price,
      render: val => <span>{`$ ${val}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</span>,
    },
    {
      title: 'Descripción',
      dataIndex: 'description',
      sorter: (a, b) => a.name.localeCompare(b.name),
    },
    {
      title: 'Acciones',
      dataIndex: 'key',
      render: val => <Button type="primary" shape="circle" icon={<DeleteOutlined />} danger onClick={() => deleteMenuItem(val)} />,
    },
  ];

  const toggleAddMenuItem = () => setShowAddMenuItem(prev => !prev);

  const deleteImages = id => {
    const [item] = menuItems.filter(item => item.key === id);
    const ext = item.images.path.split('.').pop();
    const path = item.images.path.split('.').shift();
    const paths = [`${path}.${ext}`, `${path}@480.${ext}`, `${path}@640.${ext}`];
    paths.forEach(path => {
      storage
        .ref()
        .child(path)
        .delete()
        .then(() => {
          // File deleted successfully
          console.log(path, 'deleted successfully');
        })
        .catch(error => {
          // Uh-oh, an error occurred!
          console.log(error);
        });
    });
  };

  const deleteMenuItem = id => {
    db.collection('menu')
      .doc(id)
      .delete()
      .then(() => {
        console.log('Document successfully deleted!');
        deleteImages(id);
        dispatch(removeMenuItem(id));
      })
      .catch(error => {
        console.error('Error removing document: ', error);
      });
  };

  useEffect(() => {
    const getData = async () => {
      try {
        // get data and save to state
        const MenuSnapshot = await db.collection('menu').get();
        const res = [];
        MenuSnapshot.forEach(doc => res.push({ key: doc.id, ...doc.data() }));
        dispatch(setMenu(res));
        setIsLoading(false);
      } catch (err) {
        console.error('Error on get menu: ', err);
        setIsLoading(false);
      }
    };
    getData();
  }, [dispatch]);

  useEffect(() => {
    //reactive connection
    const unsubscribe = db
      .collection('counters')
      .doc('currentTurn')
      .onSnapshot(doc => {
        console.log(doc.data());
        setCurrentTurn(doc.data().val);
      });
    return () => unsubscribe();
  }, []);

  const transaction = () => {
    const newDocRef = db.collection('queue').doc();
    const ref = db.collection('counters').doc('currentTurn');
    const user = firebase.auth().currentUser;

    db.runTransaction(t => {
      let counter;
      return t
        .get(ref)
        .then(doc => {
          if (doc.data().val === 100) counter = 1;
          else counter = doc.data().val + 1;
          return t.update(ref, { val: counter });
        })
        .then(t => {
          return t.set(newDocRef, { turn: counter, customer: user.uid, createdAt: firebase.firestore.FieldValue.serverTimestamp() });
        });
    })
      .then(result => {
        console.log(result);
      })
      .catch(err => {
        console.log('Transaction failure:', err);
      });
  };

  const signOut = () => {
    auth
      .signOut()
      .then(function() {
        console.log('Sign-out successful.');
      })
      .catch(function(error) {
        // An error happened.
      });
  };

  return (
    <div>
      {user && (
        <Button type="primary" icon={<LogoutOutlined />} onClick={signOut}>
          Salir
        </Button>
      )}

      <Row gutter={16}>
        <Col span={4}>
          <Title level={2}>Categorias</Title>
          <CategoriesNav />
        </Col>
        <Col span={20}>
          <Title level={2}>Elementos</Title>
          <Button
            type="primary"
            onClick={() => {
              setShowAddMenuItem(true);
            }}
          >
            <PlusOutlined /> Agregar nuevo elemento
          </Button>
          <Table columns={columns} dataSource={menuItems} loading={isLoading} />
          <AddMenuItem visible={showAddMenuItem} toggle={toggleAddMenuItem} />
        </Col>
      </Row>
      <Categories />
      <span>{currentTurn}</span>
      <Button onClick={transaction}>Test transaction</Button>
    </div>
  );
};
