import React from 'react';
import { Button, Form, Input, message } from 'antd';
import { LockOutlined, MailOutlined } from '@ant-design/icons';
import { auth } from '../firebase';

export default () => {
  const onFinish = async values => {
    auth
      .signInWithEmailAndPassword(values.email, values.password)
      .then(res => console.log(res))
      .catch(error => {
        console.error(error);
        message.error(`${error.message} (${error.code})`);
      });
  };

  return (
    <Form name="sign-in" onFinish={onFinish} style={{ padding: 50 }}>
      <Form.Item name="email" rules={[{ required: true, message: 'Ingresa tu correo' }]}>
        <Input prefix={<MailOutlined />} placeholder="Correo electronico" />
      </Form.Item>
      <Form.Item name="password" rules={[{ required: true, message: 'Ingresa tu contraseña' }]}>
        <Input prefix={<LockOutlined />} type="password" placeholder="Contraseña" />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button">
          Inicia sesion
        </Button>
      </Form.Item>
    </Form>
  );
};
