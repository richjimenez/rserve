import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Result, Button } from 'antd';
import { setCurrentUser } from '../redux/auth/actions';
import { db } from '../firebase.js';

export default () => {
  const [name, setName] = useState('Nombre');
  const dispatch = useDispatch();

  useEffect(() => {
    const unsubscribe = db
      .collection('menus')
      .doc('i07nHGZpjjvKHdxYYiOl')
      .onSnapshot(
        doc => {
          const { nombre, precio } = doc.data();
          setName(nombre + ' ' + precio);
          dispatch(setCurrentUser(nombre));
          console.log('Current data: ', doc.data());
        },
        err => {
          console.log(err);
        }
      );

    // returning the unsubscribe function will ensure that
    // we unsubscribe from document changes when our id
    // changes to a different value.
    return () => unsubscribe();
  }, [dispatch]);

  const prueba = val => {
    console.log('P', val.target.value);
  };

  return (
    <div className="App">
      <Result
        status="403"
        subTitle={name}
        extra={
          <Button type="primary" onClick={prueba} value="ass">
            Back Home
          </Button>
        }
      />
    </div>
  );
};
