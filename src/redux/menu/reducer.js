import menuTypes from './types';
const initState = { items: [], categories: [], categoriesTree: [] };

const menuReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case menuTypes.SET_MENU:
      return {
        ...state,
        items: payload,
      };

    case menuTypes.ADD_MENU_ITEM:
      return {
        ...state,
        items: [...state.items, payload],
      };

    case menuTypes.UPDATE_MENU_ITEM:
      return {
        ...state,
        items: state.items.map(item => {
          if (item !== payload.key) return item;
          return payload;
        }),
      };

    case menuTypes.REMOVE_MENU_ITEM:
      return {
        ...state,
        items: state.items.filter(item => payload !== item.key),
      };

    case menuTypes.SET_CATEGORIES:
      return {
        ...state,
        categories: payload,
      };

    case menuTypes.SET_CATEGORIES_TREE:
      return {
        ...state,
        categoriesTree: payload,
      };

    default:
      return state;
  }
};

export default menuReducer;
