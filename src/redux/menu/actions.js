import types from './types';

export const setMenu = menu => {
  return {
    type: types.SET_MENU,
    payload: menu,
  };
};

export const addNewMenuItem = item => {
  return {
    type: types.ADD_MENU_ITEM,
    payload: item,
  };
};

export const updateMenuItem = item => ({ type: types.UPDATE_MENU_ITEM, payload: item });

export const removeMenuItem = id => ({ type: types.REMOVE_MENU_ITEM, payload: id });

export const setCategoriesTree = categories => ({ type: types.SET_CATEGORIES_TREE, payload: categories });
export const setCategoriesTreeAsync = data => dispatch => dispatch(setCategoriesTree(data));

export const setCategories = categories => ({ type: types.SET_CATEGORIES, payload: categories });
export const setCategoriesAsync = data => dispatch => dispatch(setCategories(data));
