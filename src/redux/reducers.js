import { combineReducers } from 'redux';
import authReducer from './auth/reducer';
import menuReducer from './menu/reducer';

const reducers = combineReducers({
  auth: authReducer,
  menu: menuReducer,
});

export default reducers;
