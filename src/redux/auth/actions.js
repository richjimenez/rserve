import authTypes from './types';

export const setCurrentUser = user => ({ type: authTypes.SET_CURRENT_USER, payload: user });
