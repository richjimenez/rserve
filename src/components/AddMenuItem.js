import React, { useState } from 'react';
import { Modal, Form, Input, InputNumber, TreeSelect, Progress, Avatar, Row, Col, message } from 'antd';
import { storage, functions } from '../firebase';
import nanoid from 'nanoid';
import { db } from '../firebase';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { addNewMenuItem } from '../redux/menu/actions';

const { TextArea } = Input;

const AddMenuItem = ({ visible, toggle }) => {
  const [form] = Form.useForm();
  const categories = useSelector(state => state.menu.categoriesTree);
  const [category, setCategory] = useState(null);
  const [uploadPercent, setUploadPercent] = useState(0);
  const [uploadStatus, setUploadStatus] = useState('normal');
  const [isSaving, setIsSaving] = useState(false);
  const [img, setImg] = useState(null);
  const [imgBlob, setImgBlob] = useState(null);
  const [imgPath, setImgPath] = useState(null);
  const dispatch = useDispatch();

  console.log(category);

  const resetValues = () => {
    setImg(null);
    setImgBlob(null);
    setImgPath(null);
    setUploadPercent(0);
    setUploadStatus('normal');
    setIsSaving(false);
    toggle(); // hide modal
  };

  const handleSelectedImage = e => {
    if (e.target.files[0]) {
      const image = e.target.files[0];
      const ext = image.name.split('.').pop(); // get image extension
      const fileName = `${nanoid()}.${ext}`; // create new unique filename
      setImgPath(`menu/${fileName}`);
      setImgBlob(URL.createObjectURL(image)); // create blob to use in preview
      Object.defineProperty(image, 'name', { writable: true, value: fileName }); // rename filename
      setImg(image);
    } else {
      setImg(null);
      setImgBlob(null);
      setImgPath(null);
    }
  };

  const saveMenuItem = async values => {
    setIsSaving(true);
    const item = values;
    delete item.imagen;
    item.category = category;
    let alert;
    try {
      // save document
      const docRef = await db.collection('menu').add(item);
      // upload image to storage
      setUploadStatus('active');
      const file = img;
      const storageRef = storage.ref();
      const path = `menu/${file.name}`;
      const uploadTask = storageRef.child(path).put(file);
      uploadTask.on('state_changed', snapshot => {
        // show the upload percent
        setUploadPercent(Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100));
      });
      await uploadTask;
      setUploadStatus('success');
      resetValues();
      // create thumbs and omptimize image
      alert = message.loading('Procesando imagen...', 0);
      const optimizeImage = functions.httpsCallable('optimizeImage');
      const optimizeResult = await optimizeImage({ filePath: imgPath });
      // get urls and save to item
      const images = {};
      for (const file of optimizeResult.data) {
        const url = await storageRef.child(`menu/${file}`).getDownloadURL();
        if (file.includes('@480')) images.s = url;
        else if (file.includes('@640')) images.m = url;
        else images.o = url;
      }
      images.path = imgPath;
      await docRef.update({ images });
      alert(); // hide alert
      message.success('Imagen procesada');
      item.images = images; // add images to item obj
      dispatch(addNewMenuItem({ key: docRef.id, ...item })); // add new item to store
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <Modal
      visible={visible}
      title="Agregar nuevo elemento al menu"
      okText="Guardar"
      cancelText="Cancelar"
      onCancel={toggle}
      confirmLoading={isSaving}
      cancelButtonProps={{ disabled: isSaving }}
      maskClosable={false}
      closable={false}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            form.resetFields();
            saveMenuItem(values);
          })
          .catch(info => {
            console.log('Validate Failed:', info);
          });
      }}
    >
      <Form form={form} layout="vertical">
        <Row gutter={16}>
          <Col>
            <Avatar size={80} src={imgBlob} />
          </Col>
          <Col flex="auto">
            <Form.Item
              name="imagen"
              label="Imagen"
              rules={[
                {
                  required: true,
                  message: 'Selecciona una imagen',
                },
              ]}
            >
              <Input type="file" accept="image/x-png,image/gif,image/jpeg" onChange={handleSelectedImage} />
            </Form.Item>
          </Col>
        </Row>

        <Form.Item label="Categoria">
          <TreeSelect
            onChange={(id, [label]) => setCategory({ id, name: label })}
            treeData={categories}
            placeholder="Selecciona una categoria"
            treeDefaultExpandAll
          />
        </Form.Item>

        <Form.Item
          name="name"
          label="Nombre"
          rules={[
            {
              required: true,
              message: 'Please input the title of collection!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="price"
          label="Precio"
          rules={[
            {
              required: true,
              message: 'Ingresa un precio valido',
            },
          ]}
        >
          <InputNumber
            style={{ width: '100%' }}
            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            parser={value => value.replace(/\$\s?|(,*)/g, '')}
          />
        </Form.Item>

        <Form.Item name="description" label="Descripcion">
          <TextArea rows={4} />
        </Form.Item>

        {(uploadStatus === 'active' || uploadStatus === 'success') && (
          <Progress
            strokeColor={{
              '0%': '#108ee9',
              '100%': '#87d068',
            }}
            status={uploadStatus}
            percent={uploadPercent}
          />
        )}
      </Form>
    </Modal>
  );
};

export default AddMenuItem;
