import React, { useState, useEffect } from 'react';
import { Form, Input, Button } from 'antd';
import { db } from '../firebase';

const AddCategory = () => {
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    // const getSubCollections = functions.httpsCallable('getSubCollections');

    // getSubCollections({ docPath: 'menus/n3Ks0rlQuGI90OhM6H3R' })
    //   .then(function(result) {
    //     var collections = result.data.collections;
    //     console.log(collections);
    //   })
    //   .catch(function(error) {
    //     // Getting the Error details.
    //     var code = error.code;
    //     var message = error.message;
    //     var details = error.details;
    //     // ...
    //   });

    db.collection('menu')
      .get()
      .then(querySnapshot => {
        console.log(querySnapshot.empty);
        querySnapshot.forEach(doc => {
          // doc.data() is never undefined for query doc snapshots
          console.log(doc.id, ' => ', doc.data());
        });
      })
      .catch(error => {
        console.error('Error on get menu: ', error);
      });
  }, []);

  const onFinish = values => {
    setIsLoading(true);
    setTimeout(() => {
      db.collection('menus')
        .doc('n3Ks0rlQuGI90OhM6H3R')
        .collection(values.category)
        .doc()
        .set(values)
        .then(() => {
          console.log('La categoria se agrego con exito');
          // clear form data
          form.resetFields();
          setIsLoading(false);
        })
        .catch(error => {
          console.error('Error writing document: ', error);
          setIsLoading(false);
        });
    }, 5000);
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div>
      <Form layout="vertical" form={form} onFinish={onFinish} onFinishFailed={onFinishFailed}>
        <Form.Item label="Categoría" name="category" rules={[{ required: true, message: 'Ingresa un nombre para la categoria' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="description" label="Descripción">
          <Input.TextArea />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Guardar
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default AddCategory;
