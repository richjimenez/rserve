import React, { useState, useEffect } from 'react';
import { Form, Input, TreeSelect, Row, Col, Button, Checkbox, message } from 'antd';
import { PlusSquareOutlined } from '@ant-design/icons';
import { db } from '../firebase';
import { useDispatch, useSelector } from 'react-redux';
import { setCategoriesAsync, setCategoriesTreeAsync } from '../redux/menu/actions';

const nest = (items, id = null, link = 'parent') =>
  items.filter(item => item[link] === id).map(item => ({ ...item, children: nest(items, item.id) }));

const Categories = () => {
  const [isSub, setIsSub] = useState(false);
  const [parent, setParent] = useState(null);
  const categories = useSelector(state => state.menu.categoriesTree);
  const dispatch = useDispatch();

  // const getData = async () => {
  //   const snapshot = await db.collection('categories').get();
  //   const categories = snapshot.docs.map(doc => ({ id: doc.id, value: doc.id, label: doc.data().name, ...doc.data() }));
  //   setCategories(nest(categories));
  // };

  useEffect(() => {
    const unsubscribe = db.collection('categories').onSnapshot(querySnapshot => {
      const res = [];
      querySnapshot.forEach(doc =>
        res.push({ id: doc.id, value: doc.id, key: doc.id, label: doc.data().name, title: doc.data().name, ...doc.data() })
      );
      dispatch(setCategoriesTreeAsync(nest(res)));
      dispatch(setCategoriesAsync(res));
    });
    return () => unsubscribe();
  }, [dispatch]);

  const onFinish = async data => {
    try {
      if (isSub) {
        if (parent) {
          await db.collection('categories').add({ name: data.category, parent: parent });
          message.success('Categoria guardada');
        } else message.error('Selecciona una categoria padre');
      } else {
        await db.collection('categories').add({ name: data.category, parent: null });
        message.success('Categoria guardada');
      }
    } catch (err) {
      console.log(err);
      message.success('Hubo un error intenta de nuevo');
    }
  };

  return (
    <div>
      <Form layout="vertical" onFinish={onFinish} size="middle">
        <Row gutter={16}>
          {isSub && (
            <Col span={9}>
              <Form.Item>
                <TreeSelect
                  onChange={val => setParent(val)}
                  treeData={categories}
                  placeholder="Selecciona una categoria padre"
                  treeDefaultExpandAll
                />
              </Form.Item>
            </Col>
          )}
          <Col span={9}>
            <Form.Item name="category" rules={[{ required: true, message: 'Agrega el nombre de la categoria' }]}>
              <Input prefix={<PlusSquareOutlined />} placeholder="Nueva Categoria" />
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item>
              <Form.Item name="subcategory" valuePropName="checked" onChange={e => setIsSub(e.target.checked)}>
                <Checkbox>Es Subcategoría?</Checkbox>
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item>
              <Button type="primary" htmlType="submit" block>
                Agregar Categoria
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </div>
  );
};

export default Categories;
