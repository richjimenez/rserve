import React from 'react';
import { Menu } from 'antd';
import { useHistory, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';

const { SubMenu } = Menu;

export default () => {
  const categories = useSelector(state => state.menu.categoriesTree);
  const history = useHistory();
  const { category } = useParams();

  const handleLink = e => {
    if (e.key === 'all') return history.push(`/menu`);
    // condition prevents redirect when is the same category id
    if (category !== e.key) history.push(`/menu/${e.key}`);
  };

  const getMenu = data => {
    return data.map((category, i) => {
      if (category.children.length >= 1) {
        return (
          <SubMenu key={category.id} title={<span>{category.name}</span>} onTitleClick={handleLink}>
            {getMenu(category.children)}
          </SubMenu>
        );
      } else {
        return <Menu.Item key={category.id}>{category.name}</Menu.Item>;
      }
    });
  };

  return (
    <Menu mode="vertical" onClick={handleLink} selectable={false}>
      <Menu.Item key="all">Todas</Menu.Item>
      {getMenu(categories)}
    </Menu>
  );
};
