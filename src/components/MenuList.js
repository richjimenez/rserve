import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Table, Button, Avatar, Row, Col, Tree } from 'antd';
import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { db, storage } from '../firebase';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { setMenu, removeMenuItem } from '../redux/menu/actions';

import AddMenuItem from './AddMenuItem';
import Categories from './Categories';

const MenuList = () => {
  const { category } = useParams();
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(true);
  const [showAddMenuItem, setShowAddMenuItem] = useState(false);
  const dispatch = useDispatch();
  const menuItems = useSelector(state => state.menu.items);
  const categories = useSelector(state => state.menu.categories);

  const treeData = [
    {
      title: 'parent 1',
      children: [
        {
          title: 'leaf',
        },
        {
          title: 'mmm',
          children: [
            {
              title: 'leaf',
            },
            {
              title: 'leaf',
            },
          ],
        },
      ],
    },
  ];

  const columns = [
    {
      title: 'Imagen',
      dataIndex: 'key',
      render: val => {
        const [item] = menuItems.filter(item => item.key === val);
        return <Avatar shape="square" size="large" src={item.images.s} />;
      },
    },
    {
      title: 'Nombre',
      dataIndex: 'name',
      sorter: (a, b) => a.name.localeCompare(b.name),
    },
    {
      title: 'Precio',
      dataIndex: 'price',
      sorter: (a, b) => a.price - b.price,
      render: val => <span>{`$ ${val}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</span>,
    },
    {
      title: 'Descripción',
      dataIndex: 'description',
      sorter: (a, b) => a.name.localeCompare(b.name),
    },
    {
      title: 'Acciones',
      dataIndex: 'key',
      render: val => <Button type="primary" shape="circle" icon={<DeleteOutlined />} danger onClick={() => deleteMenuItem(val)} />,
    },
  ];

  const toggleAddMenuItem = () => setShowAddMenuItem(prev => !prev);

  const deleteImages = id => {
    const [item] = menuItems.filter(item => item.key === id);
    const ext = item.images.path.split('.').pop();
    const path = item.images.path.split('.').shift();
    const paths = [`${path}.${ext}`, `${path}@480.${ext}`, `${path}@640.${ext}`];
    paths.forEach(path => {
      storage
        .ref()
        .child(path)
        .delete()
        .then(() => {
          // File deleted successfully
          console.log(path, 'deleted successfully');
        })
        .catch(error => {
          // Uh-oh, an error occurred!
          console.log(error);
        });
    });
  };

  const deleteMenuItem = id => {
    db.collection('menu')
      .doc(id)
      .delete()
      .then(() => {
        console.log('Document successfully deleted!');
        deleteImages(id);
        dispatch(removeMenuItem(id));
      })
      .catch(error => {
        console.error('Error removing document: ', error);
      });
  };

  useEffect(() => {
    // normal fetch
    if (menuItems.length === 0 && isLoading) {
      console.log('Fetching data from DB');
      db.collection('menu')
        .get()
        .then(querySnapshot => {
          const res = [];
          querySnapshot.forEach(doc => res.push({ key: doc.id, ...doc.data() }));
          dispatch(setMenu(res));
          setIsLoading(false);
        })
        .catch(error => {
          console.error('Error on get menu: ', error);
        });
    } else {
      setIsLoading(false);
    }

    // reactive connection
    // const unsubscribe = db.collection('menu').onSnapshot(querySnapshot => {
    //   const res = [];
    //   querySnapshot.forEach(doc => res.push({ key: doc.id, ...doc.data() }));
    //   console.log(res);
    //   dispatch(setMenu(res));
    //   setIsLoading(false);
    // });
    // return () => unsubscribe();
  }, [dispatch, isLoading, menuItems]);

  return (
    <div>
      <Row gutter={16}>
        <Col span={9}>
          <Tree showIcon defaultExpandAll treeData={categories} onSelect={val => history.push(`/menu/${val}`)} />
        </Col>
        <Col span={9}>Tst</Col>
      </Row>
      <Categories />
      <Button
        type="primary"
        onClick={() => {
          setShowAddMenuItem(true);
        }}
      >
        <PlusOutlined /> Agregar nuevo elemento
      </Button>
      <Table columns={columns} dataSource={menuItems} loading={isLoading} />
      <AddMenuItem visible={showAddMenuItem} toggle={toggleAddMenuItem} />
    </div>
  );
};

export default MenuList;
