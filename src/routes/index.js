import React, { useEffect } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { auth } from '../firebase';
import { useDispatch } from 'react-redux';
import { setCurrentUser } from '../redux/auth/actions';

import NoMatch from '../pages/NoMatch';
import Layout from '../pages/Layout';
import SignUp from '../pages/SignUp';
import SignIn from '../pages/SignIn';

function WillMatch() {
  return <h3>Matched!</h3>;
}

export default () => {
  const dispatch = useDispatch();

  useEffect(() => {
    auth.onAuthStateChanged(user => {
      dispatch(setCurrentUser(user));
    });
  }, [dispatch]);

  return (
    <Switch>
      <Route exact path="/">
        <Redirect to="/menu" />
      </Route>
      <Route exact path="/menu">
        <Layout />
      </Route>
      <Route path="/menu/:category">
        <Layout />
      </Route>
      <Route exact path="/signin">
        <SignIn />
      </Route>
      <Route exact path="/signup">
        <SignUp />
      </Route>
      <Route path="/old-match">
        <Redirect to="/will-match" />
      </Route>
      <Route path="/will-match">
        <WillMatch />
      </Route>
      <Route path="*">
        <NoMatch />
      </Route>
    </Switch>
  );
};
